const express = require('express');
const path = require('path');
const multer = require('multer');
const loginController = require('../app/Auth/Admin/Controllers/FormController');
const authentication = require('../app/Auth/Admin/Middleware/Authentication');
const userController = require('../app/Users/Controllers/UserController');
const validator = require('../app/Auth/Admin/Middleware/Validator');
const productController = require('../app/Products/Admin/Controllers/productController');
const productTypeController = require('../app/Products/Admin/Controllers/productTypeController');
const postController = require('../app/Posts/Admin/Controllers/postController');
const categoryController = require('../app/Posts/Admin/Controllers/categoryController');
const tagController = require('../app/Posts/Admin/Controllers/tagController');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage });


const router = express.Router();
// Register Route
router.get('/register', authentication.verifynotAuthentication, (req, res, next) => {
  res.render('app/admin/login/register');
});

router.post('/register', validator.checkRegister, loginController.registerMethod);
// Login Route
router.get('/login', authentication.verifynotAuthentication, (req, res, next) => {
  res.render('app/admin/login/login');
});
router.post('/login', validator.checkLogin, loginController.loginMethod);

// Logout Route
router.get('/logout', loginController.logoutMethod);

router.get('/', authentication.verifyAuthentication, userController.userCount);

router.get('/dashboard-2', authentication.verifyAuthentication, (req, res, next) => {
  res.render('app/admin/index/index2', { title: 'AdminLTE 3 | Dashboard 2' });
});

router.get('/dashboard-3', authentication.verifyAuthentication, (req, res, next) => {
  res.render('app/admin/index/index3', { title: 'AdminLTE 3 | Dashboard 3' });
});

router.get('/pages/forms/general', authentication.verifyAuthentication, (req, res, next) => {
  res.render('app/admin/forms/general', { title: 'AdminLTE 3 | General Form Elements' });
});

// User Management Routes
router.get('/users', authentication.verifyAuthentication, userController.read);

router.get('/users/:slug', authentication.verifyAuthentication, userController.userProfile);
router.put('/users/edit/:slug', authentication.verifyAuthentication, userController.update);
router.delete('/users/delete/:slug', authentication.verifyAuthentication, userController.destroy);
// Product Type Management Routes
router.post('/product-type/create', authentication.verifyAuthentication, productTypeController.create);
router.get('/product-types', authentication.verifyAuthentication, productTypeController.read);
router.put('/product-type/edit/:slug', authentication.verifyAuthentication, productTypeController.update);
router.delete('/product-type/delete/:slug', authentication.verifyAuthentication, productTypeController.destroy);
// Product Management Routes
router.get('/products', authentication.verifyAuthentication, productController.readAll);
router.get('/product/:type_slug/:product_slug', authentication.verifyAuthentication, productController.readOne);
router.put('/products/edit/:slug', authentication.verifyAuthentication, productController.update);
router.delete('/products/delete/:slug', authentication.verifyAuthentication, productController.destroy);
// Post Category Management Routes
router.post('/category/create', authentication.verifyAuthentication, categoryController.create);
router.get('/categories', authentication.verifyAuthentication, categoryController.read);
router.put('/category/edit/:category_slug', authentication.verifyAuthentication, categoryController.update);
router.delete('/category/delete/:category_slug', authentication.verifyAuthentication, categoryController.destroy);
// Post Management Routes
router.get('/posts', authentication.verifyAuthentication, postController.readAll);
router.put('/post/edit/:post_slug', authentication.verifyAuthentication, postController.update);
router.delete('/post/delete/:post_slug', authentication.verifyAuthentication, postController.destroy);
// Post Tag Management Routes
router.get('/tags', authentication.verifyAuthentication, tagController.adminReadAll);
router.put('/tag/edit/:tag_name', authentication.verifyAuthentication, tagController.adminUpdate);
router.delete('/tag/delete/:tag_name', authentication.verifyAuthentication, tagController.adminDestroy);
module.exports = router;
