const express = require('express');
const path = require('path');
const multer = require('multer');

const clientAuthentication = require('../app/Auth/Client/Middlewares/clientAuthentication');
const postController = require('../app/Posts/Client/Controllers/postController');
const categoryController = require('../app/Posts/Client/Controllers/categoryController');
const productController = require('../app/Products/Client/Controllers/productController');
const productTypeController = require('../app/Products/Client/Controllers/productTypeController');
const tagController = require('../app/Posts/Client/Controllers/tagController');
const AuthController = require('../app/Auth/Client/Controllers/AuthController');
const validator = require('../app/Auth/Client/Middlewares/Validator');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './public/images/uploads');
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage });
const router = express.Router();

/* Login and Register Routes */
router.get('/login', clientAuthentication.verifynotAuthentication, (req, res, next) => res.render('app/client/account/login'));
router.post('/login', validator.checkLogin, AuthController.clientLoginMethod);
router.get('/register', (req, res, next) => res.render('app/client/account/register'));
router.post('/register', validator.checkRegister, AuthController.clientRegisterMethod);
router.get('/logout', AuthController.clientLogoutMethod);
/* Show homepage */
router.get('/', postController.clientReadAll);
/* Show Category */
router.get('/category/:category_slug', categoryController.clientReadAll);
/* Show post */
router.get('/post/:category_slug/:post_slug', postController.clientReadOne);
router.get('/post/create', clientAuthentication.verifyAuthentication, postController.clientCreateForm);
router.post('/post/create', [upload.single('previewImage'), clientAuthentication.verifyAuthentication], postController.clientCreateMethod);
router.post('/post/image-upload', [upload.single('postImage'), clientAuthentication.verifyAuthentication], postController.clientTinyImage);
/* Show posts in Tags */
router.get('/tag/:tag_name', clientAuthentication.verifyAuthentication, tagController.clientReadAll);
/* Show products in Product Type */
router.get('/product-type/:product_type_slug', productTypeController.clientReadAll);
/* Show product */
router.get('/product/create', clientAuthentication.verifyAuthentication, productController.clientCreateForm);
router.post('/product/create', [upload.array('productImage', 12), clientAuthentication.verifyAuthentication], productController.clientCreateMethod);
router.get('/products', productController.clientReadAll);
router.get('/product/:product_type_slug/:product_slug', productController.clientReadOne);
module.exports = router;
