tinymce.init({
    selector: 'textarea#basic-example',
    height: 500,
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/post/image-upload');
        xhr.onload = function() {
            let json;
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
            json = JSON.parse(xhr.responseText);
            if (!json || typeof json != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
            success(json);
        };
        formData = new FormData();
        formData.append('postImage', blobInfo.blob(), blobInfo.filename());
        xhr.send(formData);
    },
    menubar: true,
    plugins: [
        'image code',
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontselect fontsizeselect forecolor backcolor |  removeformat | help | image code',
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tiny.cloud/css/codepen.min.css'
    ],
    image_class_list: [
        {title: 'Responsive', value: 'img-fluid'}
    ],
});