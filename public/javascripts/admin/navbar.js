$(function() {
    const currentpath = window.location.pathname;
    $('.item-active-control').each(function(i,obj){
        const href = $(this).children().attr('href');
        if(href === currentpath) {
            $(this).children().addClass('active');
        }
    });
});