const $inside = $('#logout').html();
function Confirm(title, msg, $true, $false, $link) {
    const $content = "<div class='dialog-ovelay'>" +
                        "<div class='dialog'><header>" +
                        " <h3> " + title + " </h3> " +
                        "<i class='fa fa-close'></i>" +
                    "</header>" +
                    "<div class='dialog-msg'>" +
                        " <p> " + msg + " </p> " +
                    "</div>" +
                    "<footer>" +
                        "<div class='controls'>" +
                            " <button class='button button-danger doAction'>" + $true + "</button> " +
                            " <button class='button button-default cancelAction'>" + $false + "</button> " +
                        "</div>" +
                    "</footer>" +
                "</div>" +
                "</div>";
     $('body').prepend($content);
  $('.doAction').click(() => {
    window.location.href = $link;
    $(this).parents('.dialog-ovelay').fadeOut(500, function () {
      $(this).remove();
    });
  });
$('.cancelAction, .fa-close').click(function () {
    $(this).parents('.dialog-ovelay').fadeOut(500, function () {
      $(this).remove();
    });
  });
}
$('#logout').click(() => {
    Confirm('You are about to ' + $inside, 'Are you sure you want to ' + $inside + '?', 'Yes', 'Cancel', "/admin/logout");
});
