require('dotenv').config();
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);

const options = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: 'root',
    database: 'meatball',
};
const sessionStore = new MySQLStore(options);
const sessionSQL = {
    key: 'cookie_name',
    secret: 'nope',
    resave: true,
    // Store session inside DB
    store: sessionStore,
    saveUninitialized: true,
};
module.exports = {
    options,
    sessionSQL,
};
