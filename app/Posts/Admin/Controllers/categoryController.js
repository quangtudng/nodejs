const getSlug = require('speakingurl');
const knex = require('../../../../database/connection');

const create = async (req, res, next) => {
    const categoryName = req.body.category_name;
    await knex('categories').insert({
        category_name: categoryName,
        category_slug: getSlug(categoryName),
    });
    return res.redirect('/admin/categories');
};
const read = async (req, res, next) => {
    const categories = await knex('categories').select('*');
    return res.render('app/admin/posts/post_category_table', { title: 'SuperAdmin | All Categories', categories });
};
const update = async (req, res, next) => {
    const slug = req.params.category_slug;
    await knex('categories')
        .update({
            category_name: req.body.category_name,
            category_slug: getSlug(req.body.category_name),
        })
        .where({
            category_slug : slug,
        });
    return res.redirect('/admin/categories');
};
const destroy = async (req, res, next) => {
    const slug = req.params.category_slug;
    await knex('categories')
        .del()
        .where({
            category_slug : slug,
        });
    return res.redirect('/admin/categories');
};
module.exports = {
    create,
    read,
    update,
    destroy,
};
