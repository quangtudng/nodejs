const getSlug = require('speakingurl');
const { s3Upload } = require('../../../../services/s3Upload');
const knex = require('../../../../database/connection');


const readAll = async (req, res, next) => {
    const { tag } = req.query;
    const query = knex('posts')
        .leftJoin('users', 'posts.author_id', 'users.id')
        .leftJoin('categories', 'posts.category_id', 'categories.id')
        .select(
            'posts.id as PID',
            'title',
            'contents',
            'posts.post_slug as postSlug',
            'users.slug as userSlug',
            'name',
            'category_name',
            'category_slug',
            'posts.created_at as postCreatedAt',
            'posts.updated_at as postUpdatedAt',
        );
        if(tag) {
            await query
            .leftJoin('post_tag', 'posts.id', 'post_tag.post_id')
            .leftJoin('tags', 'post_tag.tag_id', 'tags.id')
            .where({
                tag_name: tag,
            });
        }
        const posts = await query;
    return res.render('app/admin/posts/post_table', { title: 'SuperAdmin | Show All Posts', posts });
};
const update = async (req, res, next) => {
    const slug = req.params.post_slug;
    await knex('posts').update({
        title: req.body.title,
        post_slug: getSlug(`${req.body.title}-${Date.now()}`),
    }).where({ post_slug: slug });
    return res.redirect('/admin/posts');
};
const destroy = async (req, res, next) => {
    const slug = req.params.post_slug;
    await knex('posts').del().where({ post_slug: slug });
    return res.redirect('/admin/posts');
};
module.exports = {
    readAll,
    update,
    destroy,
};
