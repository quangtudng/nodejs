const knex = require('../../../../database/connection');

const adminReadAll = async (req, res, next) => {
    const tags = await knex('tags')
        .leftJoin('users', 'tags.author_id', 'users.id')
        .leftJoin('post_tag', 'tags.id', 'post_tag.tag_id')
        .leftJoin('posts', 'post_tag.post_id', 'posts.id')
        .select(
            'tags.id as TID',
            'tag_name',
            'name',
            'tags.created_at as tagCreatedAt',
            'tags.updated_at as tagUpdatedAt',
            'slug as userSlug',
            'title',
        )
        .groupBy('tag_name')
        .orderBy('tagCreatedAt', 'desc')
        .count('title as count');
    return res.render('app/admin/posts/post_tag_table', { title: 'SuperAdmin | Tags', tags });
};

const adminUpdate = async (req, res, next) => {
    const slug = req.params.tag_name;
    await knex('tags').update(req.body).where({ tag_name: slug });
    return res.redirect('/admin/tags');
};

const adminDestroy = async (req, res, next) => {
    const slug = req.params.tag_name;
    await knex('tags').del().where({ tag_name: slug });
    return res.redirect('/admin/tags');
};
module.exports = {
    adminReadAll,
    adminUpdate,
    adminDestroy,
};
