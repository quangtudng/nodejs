const knex = require('../../../../database/connection');

const clientReadAll = async (req, res, next) => {
    const slug = req.params.category_slug;
    const posts = await knex('categories')
        .leftJoin('posts', 'categories.id', 'posts.category_id')
        .leftJoin('users', 'posts.author_id', 'users.id')
        .leftJoin('post_images', 'posts.id', 'post_images.post_id')
        .where({
            category_slug: slug,
        })
        .select(
            'title',
            'contents',
            'posts.post_slug as postSlug',
            'users.slug as userSlug',
            'name',
            'category_name',
            'category_slug',
            'image_url',
            'posts.created_at as postCreatedAt',
            'posts.updated_at as postUpdatedAt',
        );
    const categories = await knex('categories').select(
        'category_name',
        'category_slug',
    );
    return res.render('app/client/posts/posts_by_category', { title: posts[0].category_name, posts, categories });
};
module.exports = {
    clientReadAll,
};
