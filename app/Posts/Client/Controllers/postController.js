const getSlug = require('speakingurl');
const knex = require('../../../../database/connection');
const { s3Upload } = require('../../../../services/s3Upload.js');
const { splitTag } = require('../../../../utilities/localRequest');

const clientReadAll = async (req, res, next) => {
    const posts = await knex('posts')
    .leftJoin('users', 'posts.author_id', 'users.id')
    .leftJoin('categories', 'posts.category_id', 'categories.id')
    .leftJoin('post_images', 'posts.id', 'post_images.post_id')
    .select(
        'posts.id as PID',
        'title',
        'contents',
        'posts.post_slug as postSlug',
        'users.slug as userSlug',
        'name',
        'category_name',
        'category_slug',
        'image_url',
        'posts.created_at as postCreatedAt',
        'posts.updated_at as postUpdatedAt',
    );
    const categories = await knex('categories').select(
        'category_name',
        'category_slug',
    );
    return res.render('app/client/posts/blog', { title: 'Ricardo Milos', posts, categories });
};
const clientReadOne = async (req, res, next) => {
    const categorySlug = req.params.category_slug;
    const postSlug = req.params.post_slug;
    const post = await knex('posts')
        .leftJoin('users', 'posts.author_id', 'users.id')
        .leftJoin('categories', 'posts.category_id', 'categories.id')
        .leftJoin('post_images', 'posts.id', 'post_images.post_id')
        .leftJoin('post_tag', 'posts.id', 'post_tag.post_id')
        .leftJoin('tags', 'post_tag.tag_id', 'tags.id')
        .where({
            category_slug: categorySlug,
            post_slug: postSlug,
        })
        .select(
            'title',
            'contents',
            'category_name',
            'image_url',
            'name',
            'email',
            'username',
            'tag_name',
            'posts.created_at as postCreatedAt',
            'posts.updated_at as postUpdatedAt',
            'users.created_at as userCreatedAt',
        );
    return res.render('app/client/posts/post_detail', { title: post[0].title, post });
};
const clientCreateForm = async (req, res, next) => {
    const categories = await knex('categories').select('*');
    return res.render('app/client/posts/client_post_create', { title: 'Create a post', categories });
};
const clientCreateMethod = async (req, res, next) => {
    const url = await s3Upload(req.file);
    const userID = await knex('users').where({ slug: req.session.user.slug }).first('id');
    const categoryID = await knex('categories').where({ category_name: req.body.category_name }).first('id');

    await knex('posts').insert({
        title: req.body.title,
        post_slug: getSlug(`${req.body.title}-${Date.now()}`),
        contents: req.body.contents,
        author_id: userID.id,
        category_id: categoryID.id,
    })
    .then(async (id) => {
            const postID = id;
            const tags = splitTag(req.body.tag_name);

            await knex('post_images')
                .insert({
                    post_id: postID,
                    image_url: url,
            });

            tags.forEach(async (tag) => {
                    await knex('tags')
                        .select()
                        .where({
                            tag_name: tag,
                        })
                        .then(async (rows) => {
                            if (rows.length === 0) {
                                // If there are no duplicate Tags in req.body
                                await knex('tags')
                                    .insert({
                                        author_id: userID.id,
                                        tag_name: tag,
                                    })
                                    .then(async (id) => {
                                        await knex('post_tag')
                                            .insert({
                                                post_id: postID,
                                                tag_id: id,
                                            });
                                    });
                            } else {
                                // If there are duplicate Tags in req.body
                                await knex('tags')
                                .where({
                                    tag_name: tag,
                                })
                                .first('id')
                                .then(async (data) => {
                                    await knex('post_tag')
                                        .insert({
                                            post_id: postID,
                                            tag_id: data.id,
                                        });
                                });
                            }
                        });
                });
        });
    return res.redirect('/');
};
const clientTinyImage = async (req, res, next) => {
    const { file } = req;
    if (!file) {
        return next();
    }
    const url = await s3Upload(file);
    return res.json(url);
};
module.exports = {
    clientCreateForm,
    clientCreateMethod,
    clientReadAll,
    clientReadOne,
    clientTinyImage,
};
