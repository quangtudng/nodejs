const knex = require('../../../../database/connection');

const clientReadAll = async (req, res, next) => {
    const slug = req.params.tag_name;
    const categories = await knex('categories').select(
        'category_name',
        'category_slug',
    );
    const posts = await knex('tags')
        .leftJoin('post_tag', 'tags.id', 'post_tag.tag_id')
        .leftJoin('posts', 'post_tag.post_id', 'posts.id')
        .leftJoin('users', 'posts.author_id', 'users.id')
        .leftJoin('post_images', 'posts.id', 'post_images.post_id')
        .where({
            tag_name: slug,
        })
        .select(
            'title',
            'contents',
            'posts.post_slug as postSlug',
            'users.slug as userSlug',
            'name',
            'tag_name',
            'image_url',
            'posts.created_at as postCreatedAt',
            'posts.updated_at as postUpdatedAt',
        )
        .then(posts => res.render('app/client/posts/posts_by_tag', { title: posts[0].tag_name, posts, categories }))
        .catch(err => next());
};
module.exports = {
    clientReadAll,
};
