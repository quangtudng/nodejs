const knex = require('../../../database/connection');

const read = async (req, res) => {
    const { q } = req.query;
    const query = knex('users').select('id', 'name', 'username', 'email', 'slug', 'created_at');
    if (q) {
        await query.where('username', 'like', `%${q}%`)
        .orWhere('email', 'like', `%${q}%`)
        .orWhere('name', 'like', `%${q}%`);
    }
    const users = await query;
    return res.render('app/admin/tables/simple', { title: 'AdminLTE 3 | Simple Tables', users });
};
const userProfile = async (req, res, next) => {
    const { slug } = req.params;
    const info = await knex('users').where('slug', slug).first('username', 'name', 'email', 'created_at', 'slug');
    if (info) {
        return res.render('app/admin/users/profile', {
            title: 'AdminLTE 3 | Dashboard 3',
            name: info.name,
            email: info.email,
            date_created: info.created_at,
            slug: info.slug,
});
    }
    return next();
};
const update = async (req, res, next) => {
    const data = req.body;
    const { slug } = req.params;
    await knex('users').where('slug', slug).update({
        name: data.name,
        email: data.email,
        username: data.username,
    });
    return res.redirect('/admin/users');
};
const destroy = async (req, res, next) => {
    const { slug } = req.params;
    await knex('users').where('slug', slug).del();
    return res.redirect('/admin/users');
};
const userCount = async (req, res, next) => {
    let count = await knex('users').count('id as count');
    count = count[0].count;
    return res.render('app/admin/index/index', { title: 'AdminLTE 3 | Dashboard', registereduser: count });
};
module.exports = {
    read,
    userProfile,
    update,
    destroy,
    userCount,
};
