const getSlug = require('speakingurl');
const knex = require('../../../../database/connection');
const { s3Upload } = require('../../../../services/s3Upload');
const { resizeImage } = require('../../../../utilities/localRequest');

const clientCreateForm = async (req, res, next) => {
    const productTypes = await knex('product_types').select('*');
    return res.render('app/client/products/client_product_create', { title: 'Creating a Product', productTypes });
};
const clientCreateMethod = async (req, res, next) => {
    const uploadImages = req.files;
    const productTypeID = await knex('product_types')
        .where({ product_type_name: req.body.product_type_name })
        .select('id');
    await knex('products')
        .insert({
            product_name: req.body.product_name,
            description: req.body.description,
            product_type_id: productTypeID[0].id,
            price: req.body.price,
            product_slug: getSlug(`${req.body.product_name}-${Date.now()}`),
            author_id: req.session.user.id,
        })
        .then((id) => {
            uploadImages.forEach(async (uploadImage) => {
                await resizeImage(uploadImage, 700);
                const url = await s3Upload(uploadImage);
                await knex('product_images')
                .insert({
                    image_url: url,
                    product_image_id: id,
                });
            });
        });
    return res.redirect('/products');
};

const clientReadAll = async (req, res, next) => {
    const products = await knex('products')
    .leftJoin('product_types', 'products.product_type_id', 'product_types.id')
    .leftJoin('users', 'products.author_id', 'users.id')
    .leftJoin('product_images', 'products.id', 'product_images.product_image_id')
    .select(
        'username',
        'product_name',
        'product_slug',
        'product_type_slug',
        'price',
        'image_url',
        'product_type_name',
        'products.created_at as product_created_at',
        'product_types.created_at as product_type_created_at',
    )
    .groupBy('product_slug');
    const productTypes = await knex('product_types').select('*');
    return res.render('app/client/products/products', { title: 'AdminLTE 3 | Data', products, productTypes });
};

const clientReadOne = async (req, res, next) => {
    const product = await knex('products')
        .leftJoin('users', 'products.author_id', 'users.id')
        .leftJoin('product_types', 'products.product_type_id', 'product_types.id')
        .leftJoin('product_images', 'products.id', 'product_images.product_image_id')
        .where({
            product_type_slug: req.params.product_type_slug,
            product_slug: req.params.product_slug,
        })
        .select(
            'name',
            'product_name',
            'price',
            'description',
            'products.created_at as productCreated',
            'slug',
            'product_type_name',
            'image_url',
        );
    return res.render('app/client/products/product_detail', { title: product[0].product_name, product });
};
module.exports = {
    clientCreateForm,
    clientCreateMethod,
    clientReadAll,
    clientReadOne,
};
