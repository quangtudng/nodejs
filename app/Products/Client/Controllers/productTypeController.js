const knex = require('../../../../database/connection');

const clientReadAll = async (req, res, next) => {
    const slug = req.params.product_type_slug;
    const products = await knex('product_types')
    .leftJoin('products', 'product_types.id', 'products.product_type_id')
    .leftJoin('product_images', 'products.id', 'product_images.product_image_id')
    .leftJoin('users', 'products.author_id', 'users.id')
    .where({
        product_type_slug: slug,
    })
    .select(
        'product_name',
        'product_slug',
        'price',
        'users.slug as userSlug',
        'name',
        'product_type_name',
        'product_type_slug',
        'image_url',
        'products.created_at as productCreatedAt',
    )
    .groupBy('product_slug');
    return res.render('app/client/products/products_by_type', { title: products[0].product_type_name, products });
};
module.exports = {
    clientReadAll,
};
