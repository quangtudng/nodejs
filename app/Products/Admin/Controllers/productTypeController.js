const getSlug = require('speakingurl');
const knex = require('../../../../database/connection');

const create = async (req, res, next) => {
    const productTypeName = req.body.product_type_name;

    await knex('product_types').insert({
        product_type_name: productTypeName,
        product_type_slug: getSlug(productTypeName),
    });

    return res.redirect('/admin/product-types');
};
const read = async (req, res, next) => {
    const productTypes = await knex('product_types').select('*');
    return res.render('app/admin/products/product_type_table', { title: 'AdminLTE 3 | Product Types', productTypes });
};
const update = async (req, res, next) => {
    const { slug } = req.params;

    await knex('product_types')
        .where({ product_type_slug: slug })
        .update({
            product_type_name: req.body.product_type_name,
            product_type_slug: getSlug(`${req.body.product_type_name}-${Date.now()}`),
        });

    return res.redirect('/admin/product-types');
};
const destroy = async (req, res, next) => {
    const { slug } = req.params;

    await knex('product_types').where({ product_type_slug: slug }).del();

    return res.redirect('/admin/product-types');
};
module.exports = {
    create,
    read,
    update,
    destroy,
};
