const knex = require('../../../../database/connection');

const readAll = async (req, res, next) => {
  const products = await knex('products')
  .leftJoin('product_types', 'product_type_id', 'product_types.id')
  .leftJoin('users', 'author_id', 'users.id')
  .select(
    'username',
    'users.slug as user_slug',
    'products.id as product_id',
    'product_types.id as product_type_id',
    'product_name',
    'product_slug',
    'product_type_slug',
    'description',
    'price',
    'product_type_name',
    'products.created_at as product_created_at',
    'product_types.created_at as product_type_created_at',
  );
  return res.render('app/admin/products/product_table', { title: 'AdminLTE 3 | Data', products });
};
const readOne = async (req, res, next) => {
  const productTypeSlug = req.params.type_slug;
  const slug = req.params.product_slug;
  const productData = await knex('products')
    .leftJoin('product_types', 'product_type_id', 'product_types.id')
    .leftJoin('users', 'author_id', 'users.id')
    .leftJoin('product_images', 'products.id', 'product_images.product_image_id')
    .where({
      product_slug: slug,
      product_type_slug: productTypeSlug,
    })
    .select(
      'image_url',
      'product_name',
      'price',
      'name',
      'username',
      'product_type_name',
      'description',
    );
  return res.render('app/admin/products/product_detail', { title: 'SuperAdmin | Product ', products: productData });
};
const update = async (req, res, next) => {
  const { slug } = req.params;
  const productData = req.body;
  await knex('products')
    .leftJoin('product_types', 'products.product_type_id', 'product_type', 'product_types.id')
    .where('product_slug', slug)
    .update(productData);
  return res.redirect('/admin/products');
};
const destroy = async (req, res, next) => {
  const { slug } = req.params;
  await knex('products')
  .where({
    product_slug: slug,
  })
  .del();
  return res.redirect('/admin/products');
};
module.exports = {
    readAll,
    readOne,
    update,
    destroy,
};
