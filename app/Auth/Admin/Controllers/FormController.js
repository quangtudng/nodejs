const bcrypt = require('bcrypt');
const { validationResult } = require('express-validator');
const getSlug = require('speakingurl');

const knex = require('../../../../database/connection');

const saltRounds = 10;

const registerMethod = async (req, res) => {
    const errors = validationResult(req);
    const inputData = req.body;

    const salt = bcrypt.genSaltSync(saltRounds);
    const hashPassword = bcrypt.hashSync(inputData.password, salt);

    req.session.oldvalue = inputData;

    if (!errors.isEmpty()) {
        req.flash('messages', errors.array());
        return res.redirect('/admin/register');
    }

    await knex('users').insert({
        name: inputData.name,
        email: inputData.email,
        username: inputData.username,
        password: hashPassword,
        slug: getSlug(`${inputData.username}-${Date.now()}`),
    });

    return res.redirect('/admin/login');
};

const loginMethod = async (req, res) => {
    const errors = validationResult(req);
    const inputData = req.body;

    req.session.oldvalue = inputData;

    if (!errors.isEmpty()) {
        req.flash('messages', errors.array());
        return res.redirect('/admin/login');
    }
    const getUser = await knex('users').where({
        email: inputData.email,
    }).first(
        'name',
        'password',
        'username',
        'email',
        'slug',
    );

    if (!getUser || !bcrypt.compareSync(inputData.password, getUser.password)) {
        return res.redirect('/admin/login');
    }
    if (inputData.remember === 'on') {
        req.session.cookie.expires = 999999999;
    }

    req.session.user = getUser;
    req.session.loggedIn = true;
    return res.redirect('/admin');
};

const logoutMethod = (req, res) => {
    delete req.session.user;
    delete req.session.loggedIn;
    req.session.cookie.expires = null;

    res.redirect('/admin/login');
};

module.exports = {
    loginMethod,
    registerMethod,
    logoutMethod,
};
