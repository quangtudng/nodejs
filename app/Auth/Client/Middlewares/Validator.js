const bcrypt = require('bcrypt');
const { check } = require('express-validator');
const knex = require('../../../../database/connection');

const checkRegister = [
    check('name').trim().isLength({ min: 5 }).withMessage('Please enter a name that has at least 5 characters'),
    check('username').trim().isLength({ min: 5 }).withMessage('Please enter a username that has at least 5 characters'),
    check('email').trim().isEmail().withMessage('Invalid Email'),
    check('email').custom(async (value, { req }) => {
        const email = await knex('users').where({ email: req.body.email }).first('email');
        if (email) {
            throw new Error('Email already exists!');
        }
    }),
    check('password').trim().isLength({ min: 5 }).withMessage('Invalid Password'),
    check('confirmuserpassword').custom((value, { req }) => value === req.body.password).withMessage('The confirmation password should be the same with the password'),
];
const checkLogin = [
    check('email').custom(async (value, { req }) => {
        const email = await knex('users').where({ email: req.body.email }).first('email');
        if (!email) {
            throw new Error('Email does not exist!');
        }
    }),
    check('email').trim().isEmail().withMessage('Email must not be empty!'),
    check('password').custom(async (value, { req }) => {
        const password = await knex('users').where({ email: req.body.email }).first('password');
        if (!password || !bcrypt.compareSync(req.body.password, password.password)) {
            throw new Error('Password is incorrect!');
        }
    }),
    check('password').trim().isLength({ min: 5 }).withMessage('Password must be longer than 5 letters'),
];

module.exports = { checkRegister, checkLogin };
