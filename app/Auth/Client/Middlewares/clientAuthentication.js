const knex = require('../../../../database/connection');

const verifyAuthentication = async (req, res, next) => {
    const userInfo = req.session.user;
    if (!userInfo) {
        return res.redirect('/login');
    }
    const userExist = await knex('users').where({ slug: userInfo.slug }).first('id');
    if (!userInfo || !req.session.loggedIn || typeof userExist === 'undefined') {
        return res.redirect('/logout');
    }
    next();
};
const verifynotAuthentication = (req, res, next) => {
    if (!req.session.loggedIn) {
        return next();
    }
    return res.redirect('/');
};

module.exports = {
    verifyAuthentication,
    verifynotAuthentication,
};
