const bcrypt = require('bcrypt');
const { validationResult } = require('express-validator');
const getSlug = require('speakingurl');

const knex = require('../../../../database/connection');

const saltRounds = 10;

const clientRegisterMethod = async (req, res) => {
    const errors = validationResult(req);
    const salt = bcrypt.genSaltSync(saltRounds);
    const hashPassword = bcrypt.hashSync(req.body.password, salt);
    req.session.oldvalue = req.body;
    
    if (!errors.isEmpty()) {
        req.flash('messages', errors.array());
        return res.redirect('/register');
    }

    await knex('users').insert({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: hashPassword,
        slug: getSlug(`${req.body.username}-${Date.now()}`),
    });

    return res.redirect('/login');
};

const clientLoginMethod = async (req, res) => {
    const errors = validationResult(req);

    req.session.oldvalue = req.body;

    if (!errors.isEmpty()) {
        req.flash('messages', errors.array());
        return res.redirect('/login');
    }
    const getUser = await knex('users').where({
        email: req.body.email,
    }).first(
        'id',
        'name',
        'password',
        'username',
        'email',
        'slug',
    );

    if (!getUser || !bcrypt.compareSync(req.body.password, getUser.password)) {
        return res.redirect('/login');
    }
    if (req.body.remember === 'on') {
        req.session.cookie.expires = 999999999;
    }

    req.session.user = getUser;
    req.session.loggedIn = true;
    return res.redirect('/');
};

const clientLogoutMethod = (req, res) => {
    delete req.session.user;
    delete req.session.loggedIn;
    req.session.cookie.expires = null;

    res.redirect('/');
};

module.exports = {
    clientRegisterMethod,
    clientLoginMethod,
    clientLogoutMethod,
};
