const fs = require('fs');
const sharp = require('sharp');

const resizeImage = (file, width, height) => {
    const buffer = fs.readFileSync(file.path);
    if (width || height) {
        return sharp(buffer).resize(width, height).toFile(file.path);
    }
};
const splitTag = (string) => {
    const splited = string.split(' ');
    return splited;
};
module.exports = {
    resizeImage,
    splitTag,
};
