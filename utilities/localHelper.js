const moment = require('moment');

module.exports = (req, res) => {
    res.locals.oldvalue = req.session.oldvalue;
    res.locals.errormessage = req.session.flash;
    delete req.session.oldvalue;
    delete req.session.flash;

    res.locals.valuehandling = (key) => {
        const { oldvalue } = res.locals;
        const msg = '';
        if (oldvalue) {
            return oldvalue[key];
    }
    return msg;
    };

    res.locals.errorhandling = (key) => {
        const errors = res.locals.errormessage;
        let msg = '';
        if (errors) {
            Object.values(errors).forEach((value) => {
                Object.values(value).forEach((error) => {
                    if (error.param === key) {
                        msg = error.msg;
                    }
                });
            });
            return msg;
        }
        return msg;
    };
    res.locals.moment = moment;
    res.locals.userinfo = req.session.user;
    res.locals.loggedIn = req.session.loggedIn;
};
