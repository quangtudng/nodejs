require('dotenv').config();
const aws = require('aws-sdk');
const fs = require('fs');

const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.AWS_SECRET_KEY;
aws.config.update({
    secretAccessKey,
    accessKeyId,
    region: 'ap-southeast-1',
});
const s3 = new aws.S3();

const s3Upload = async (file) => {
    const params = {
        Bucket: 'quangtu-bucket',
        Key: `tunguyen/${file.filename.toLowerCase()}`,
        Body: fs.readFileSync(`${file.path}`),
        ContentType: file.mimetype,
        ACL: 'public-read',
    };
    const data = await s3.upload(params).promise(); // (?)
    fs.unlinkSync(`${file.path}`);
    return data.Location;
};
module.exports = { s3Upload };
