exports.up = function (knex) {
    return knex.schema
    .createTable('posts', (table) => {
        table.increments('id').primary();
        table.string('title').notNullable();
        table.string('post_slug').notNullable().unique();
        table.longtext('contents').notNullable();
        table.integer('author_id').unsigned();
        table.foreign('author_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
        table.integer('category_id').unsigned();
        table.foreign('category_id').references('id').inTable('categories').onDelete('CASCADE').onUpdate('CASCADE');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('posts');
};
