exports.up = function (knex) {
    return knex.schema
    .createTable('product_type_images', (table) => {
        table.increments('id').primary();
        table.string('image_url', 255).notNullable().unique();
        table.integer('product_type_image_id').unsigned();
        table.foreign('product_type_image_id').references('id').inTable('product_types').onDelete('CASCADE').onUpdate('CASCADE');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('product_type_images');
};
