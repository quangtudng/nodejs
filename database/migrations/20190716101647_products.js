
exports.up = function (knex) {
    return knex.schema
    .createTable('products', (table) => {
        table.increments('id').primary();
        table.string('product_name', 255).notNullable();
        table.string('product_slug', 255).notNullable().unique();
        table.decimal('price', 12).notNullable();
        table.string('description').notNullable();
        table.integer('author_id').unsigned();
        table.foreign('author_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
        table.integer('product_type_id').unsigned();
        table.foreign('product_type_id').references('id').inTable('product_types').onDelete('CASCADE').onUpdate('CASCADE');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('products');
};
