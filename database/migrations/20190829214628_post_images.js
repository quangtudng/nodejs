exports.up = function (knex) {
    return knex.schema
    .createTable('post_images', (table) => {
        table.increments('id').primary();
        table.string('image_url', 255).notNullable().unique();
        table.integer('post_id').unsigned();
        table.foreign('post_id').references('id').inTable('posts').onDelete('CASCADE').onUpdate('CASCADE');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('post_images');
};
