exports.up = function (knex) {
    return knex.schema
    .createTable('post_tag', (table) => {
        table.integer('tag_id').unsigned();
        table.foreign('tag_id').references('id').inTable('tags').onDelete('CASCADE').onUpdate('CASCADE');
        table.integer('post_id').unsigned();
        table.foreign('post_id').references('id').inTable('posts').onDelete('CASCADE').onUpdate('CASCADE');
        });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('post_tag');
};
