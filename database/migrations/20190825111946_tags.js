exports.up = function (knex) {
    return knex.schema
    .createTable('tags', (table) => {
        table.increments('id').primary();
        table.string('tag_name', 255).notNullable().unique();
        table.integer('author_id').unsigned();
        table.foreign('author_id').references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
};

exports.down = function (knex) {
    return knex.schema
    .dropTable('tags');
};
